package axuan25.reactivePlant.user.api

import play.api.libs.json.{Format, Json}

import scala.collection.immutable.Seq

case class User (
  name: String,
  powerPlants: Option[Seq[Long]]) {

  def this(name: String) = this(name, None)
}

object User {
  implicit val format: Format[User] = Json.format
}
