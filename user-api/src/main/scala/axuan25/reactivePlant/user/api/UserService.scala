package axuan25.reactivePlant.user.api

import akka.NotUsed
import com.lightbend.lagom.scaladsl.api.transport.Method
import com.lightbend.lagom.scaladsl.api.{Descriptor, Service, ServiceCall}

import scala.collection.immutable.Seq


trait UserService extends Service {

  def createUser: ServiceCall[User, User]

  def getUsers: ServiceCall[NotUsed, Seq[User]]

  def getUser(userId: String): ServiceCall[NotUsed, User]

//  def getPowerPlants(userId: String): ServiceCall[NotUsed, Seq[Long]]

  override def descriptor(): Descriptor = {
    import Service._
    named("user-service").withCalls(
      restCall(Method.POST, "/api/users", createUser),
      pathCall("/api/users", getUsers _),
      pathCall("/api/users/:id", getUser _)
//      pathCall("/api/users/:id/plants", getPowerPlants _)
    ).withAutoAcl(true)
  }
}
