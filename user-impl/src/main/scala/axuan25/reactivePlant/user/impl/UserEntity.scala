package axuan25.reactivePlant.user.impl

import akka.Done
import axuan25.reactivePlant.user.api.User
import com.lightbend.lagom.scaladsl.persistence.PersistentEntity
import com.lightbend.lagom.scaladsl.persistence.PersistentEntity.ReplyType
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import play.api.libs.json.{Format, Json}

import scala.collection.immutable.Seq

class UserEntity extends PersistentEntity {

  override type Command = UserCommand[_]
  override type Event = UserEvent
  override type State = Option[User]
  override def initialState = None

  /**
    * Selects the appropriate FSM logic according to the entity state
    * @return
    */
  override def behavior: Behavior = {
    case Some(user) => existing
    case None => newEntity
  }

  /**
    * The FSM to return for new or non-existing user entities
    */
  def newEntity: Actions = {
    Actions()
      // Empty state
      .onReadOnlyCommand[GetUser.type, Option[User]] {
        case (GetUser, ctx, state) => ctx.reply(state)
      }
      // Valid command
      .onCommand[CreateUser, Done] {
        case (CreateUser(name), ctx, state) =>
          ctx.thenPersist(UserCreated(name))(_ => ctx.reply(Done))
      }
      .onEvent {
        case (UserCreated(name), state) => {
          Some(User(name, None))
        }
      }
  }

  /**
    * The FSM to return for existing user entities
    */
  def existing: Actions = {
    Actions()
      .onReadOnlyCommand[GetUser.type, Option[User]] {
      case (GetUser, ctx, state) => ctx.reply(state)
    }
      .onReadOnlyCommand[CreateUser, Done] {
      case (CreateUser(name), ctx, state) => ctx.invalidCommand("User already exists")
    }
  }
}

/*
Commands
 */

sealed trait UserCommand[R] extends ReplyType[R]

final case class CreateUser(name: String) extends UserCommand[Done]
object CreateUser {
  implicit val format: Format[CreateUser] = Json.format
}

object GetUser extends UserCommand[Option[User]] {
  implicit val format: Format[GetUser.type] = JsonSerializer.emptySingletonFormat(GetUser)
}

/**
  * Format for the GetUserPowerPlants message command.
  *
  * Persistent entities get sharded across the cluster. This means commands
  * may be sent over the network to the node where the entity lives if the
  * entity is not on the same node that the command was issued from. To do
  * that, a JSON format needs to be declared so the command can be serialized
  * and deserialized.
  */
case object GetUserPowerPlants extends UserCommand[Option[Seq[Long]]] {
  implicit val format: Format[GetUserPowerPlants.type] = JsonSerializer.emptySingletonFormat(GetUserPowerPlants)
}


/*
Events
 */

sealed trait UserEvent

final case class UserCreated(name: String) extends UserEvent
object UserCreated {
  implicit val format: Format[UserCreated] = Json.format
}

/**
  * Akka serialization, used by both persistence and remoting, needs to have
  * serializers registered for every type serialized or deserialized. While it's
  * possible to use any serializer you want for Akka messages, out of the box
  * Lagom provides support for JSON, via this registry abstraction.
  *
  * The serializers are registered here, and then provided to Lagom in the
  * application loader.
  */
object UserSerializerRegistry extends JsonSerializerRegistry {
  override def serializers: Seq[JsonSerializer[_]] = Seq(

    // Commands
    JsonSerializer[CreateUser],
    JsonSerializer[GetUser.type],
    JsonSerializer[GetUserPowerPlants.type],

    // Events
    JsonSerializer[UserCreated]
  )
}
