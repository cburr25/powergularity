package axuan25.reactivePlant.user.impl

import akka.actor.ActorSystem
import akka.persistence.cassandra.query.scaladsl.CassandraReadJournal
import akka.persistence.query.PersistenceQuery
import akka.stream.Materializer
import akka.stream.scaladsl.Sink
import axuan25.reactivePlant.user.api.{User, UserService}
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.api.transport.NotFound
import com.lightbend.lagom.scaladsl.persistence.PersistentEntityRegistry

import scala.concurrent.ExecutionContext

class UserServiceImpl(registry: PersistentEntityRegistry, system: ActorSystem)(implicit ec: ExecutionContext, mat: Materializer) extends UserService {

  override def createUser = ServiceCall { createUser =>
//    val userId = UUID.randomUUID()
    refFor(createUser.name).ask(CreateUser(createUser.name)).map { _ =>
      User(createUser.name, None)
    }
  }

  override def getUser(userId: String) = ServiceCall { _ =>
    refFor(userId).ask(GetUser).map {
      case Some(user) => user
      case None => throw NotFound(s"User with id $userId does not exist!")
    }
  }

  private val currentIdsQuery = PersistenceQuery(system).readJournalFor[CassandraReadJournal](CassandraReadJournal.Identifier)

  /**
    * TODO: implement cassandra read side
    * Demo this method too
    * @return
    */
  override def getUsers = ServiceCall { _ =>
    // Note this should never make production....
    currentIdsQuery.currentPersistenceIds()
      .filter(_.startsWith("UserEntity|"))
      .mapAsync(4) { id =>
        val entityId = id.split("\\|", 2).last
        registry.refFor[UserEntity](entityId)
          .ask(GetUser)
          .map(_.map(user => User(user.name, user.powerPlants)))
      }.collect {
      case Some(user) => user
    }
      .runWith(Sink.seq)
  }

//  private def refFor(userId: UUID) = registry.refFor[UserEntity](userId.toString)
  private def refFor(userId: String) = registry.refFor[UserEntity](userId)
}
