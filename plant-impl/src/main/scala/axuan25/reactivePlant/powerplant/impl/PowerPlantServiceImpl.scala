package axuan25.reactivePlant.powerplant.impl

import java.util.UUID

import akka.actor.ActorSystem
import akka.persistence.cassandra.query.scaladsl.CassandraReadJournal
import akka.persistence.query.PersistenceQuery
import akka.stream.Materializer
import akka.stream.scaladsl.Sink
import akka.{Done, NotUsed}
import axuan25.reactivePlant.powerplant.api.{Kilowatts, PowerPlant, PowerPlantService}
import com.lightbend.lagom.scaladsl.api.ServiceCall
import com.lightbend.lagom.scaladsl.api.transport.{NotFound, PolicyViolation, TransportErrorCode, TransportException}
import com.lightbend.lagom.scaladsl.persistence.PersistentEntity.ReplyType
import com.lightbend.lagom.scaladsl.persistence.PersistentEntityRegistry

import scala.collection.immutable
import scala.concurrent.ExecutionContext

/**
  * Created by christopher on 18/04/17.
  */
class PowerPlantServiceImpl(registry: PersistentEntityRegistry, system: ActorSystem)(implicit ec: ExecutionContext, mat: Materializer) extends PowerPlantService {

  override def create = ServiceCall { plant =>
    val id = UUID.randomUUID()
    val newPlant = plant.copy(id = Some(id))
    refFor(id).ask(CreatePowerPlant(newPlant)).map { _ => newPlant }
  }

  // This "get all" hack was taken from the "online-auction-scala" demo
  // TODO: A repository with a view model would be more appropriate
  private val currentIdsQuery = PersistenceQuery(system).readJournalFor[CassandraReadJournal](CassandraReadJournal.Identifier)

  /*
  * This non-optimal way of retrieving all users consists in loading all persistent entities from the actor system
  * Source: online-auction-scala demo
  */
  override def getAll: ServiceCall[NotUsed, immutable.Seq[PowerPlant]] = ServiceCall { _ =>
    // Note this should never make production....
    currentIdsQuery.currentPersistenceIds()
      .filter(_.startsWith("PowerPlantEntity|"))
      .mapAsync(4) { id =>
        val entityId = id.split("\\|", 2).last
        registry.refFor[PowerPlantEntity](entityId)
          .ask(GetPowerPlant)
          .map(_.map(plant => plant))
      }.collect {
      case Some(user) => user
    }
    .runWith(Sink.seq)
  }

  override def getOne(plantId: UUID): ServiceCall[NotUsed, PowerPlant] = ServiceCall { _ =>
    refFor(plantId).ask(GetPowerPlant).map {
      case Some(powerPlant) => powerPlant
      case None => throw NotFound(s"Plant with id ${plantId.toString} does not exist!")
    }
  }

  override def updatePlantCapacity(plantId: UUID): ServiceCall[Kilowatts, Done] = ServiceCall { newCapacity =>
    refFor(plantId).ask(ChangeEnergyCapacity(newCapacity))
  }

  override def produceEnergy(plantId: UUID): ServiceCall[NotUsed, Done] = ServiceCall { _ =>
    refFor(plantId).ask(ProduceEnergy)
  }

  override def consumeEnergy(plantId: UUID): ServiceCall[Kilowatts, Done] = ServiceCall { power =>
    refFor(plantId).ask(ConsumeEnergy(power))
  }

  private def refFor(plantId: UUID) = registry.refFor[PowerPlantEntity](plantId.toString)
}
