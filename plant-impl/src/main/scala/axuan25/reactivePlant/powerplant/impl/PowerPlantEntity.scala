package axuan25.reactivePlant.powerplant.impl

import java.time.Instant

import akka.Done
import axuan25.reactivePlant.powerplant.api.{Kilowatts, PowerPlant}
import com.lightbend.lagom.scaladsl.api.transport.{ExceptionMessage, PolicyViolation, TransportErrorCode, TransportException}
import com.lightbend.lagom.scaladsl.persistence.PersistentEntity
import com.lightbend.lagom.scaladsl.persistence.PersistentEntity.ReplyType
import com.lightbend.lagom.scaladsl.playjson.{JsonSerializer, JsonSerializerRegistry}
import play.api.libs.json.{Format, Json}

import scala.collection.immutable.Seq

/**
  * Created by christopher on 18/04/17.
  */
class PowerPlantEntity extends PersistentEntity {

  override type Command = PowerPlantCommand[_]
  override type Event = PowerPlantEvent
  override type State = PowerPlantState
  override def initialState = PowerPlantState.newEntity

  /**
    * Selects the appropriate Finite State Machine logic according to the entity state
    *
    * TODO/idea: "start" + "stop" plant (eg: cannot stop nuclear reactor!)
    * TODO/idea: "broken" status ? (happens when you charge over capacity?)
    */
  override def behavior: Behavior = {
    case PowerPlantState(None, _) => newEntity
    case PowerPlantState(Some(plant), _) => existingEntity
  }

  /**
    * The FSM to return for new or non-existing user entities
    */
  def newEntity: Actions = {
    Actions()
      // Empty state: None
      .onReadOnlyCommand[GetPowerPlant.type, Option[PowerPlant]] {
        case (GetPowerPlant, ctx, state) => ctx.reply(state.powerPlant)
      }
      // TODO/fixme: validation is duplicated, the better solution is to accept a partial json object and use defaults, leave the validation to its respective commands
      // TODO: on the other hand, the same validation rules could result in different error messages
      .onCommand[CreatePowerPlant, Done] {
        case (CreatePowerPlant(plant), ctx, state) if plant.capacity < plant.energy =>
          throw BadRequest("Capacity cannot be smaller than energy reserve!")
        case (CreatePowerPlant(plant), ctx, state) if plant.capacity <= plant.generator.rate =>
          throw BadRequest(s"Capacity must be equal or greater than the production rate of the generator (${plant.generator.getClass.getSimpleName}: ${plant.generator.rate}).")
        case (CreatePowerPlant(plant), ctx, state) =>
          ctx.thenPersist(PowerPlantedCreated(plant))(_ => ctx.reply(Done))
      }
      .onEvent {
        case (PowerPlantedCreated(plant), state) => state.copy(powerPlant = Some(plant))
      }
  }

  /**
    * The FSM to return for existing user entities
    */
  def existingEntity: Actions = {
    Actions()
      /*
      Get or Create
       */
      .onReadOnlyCommand[GetPowerPlant.type, Option[PowerPlant]] {
        case (GetPowerPlant, ctx, state) => ctx.reply(state.powerPlant)
      }
      .onReadOnlyCommand[CreatePowerPlant, Done] {
        case (CreatePowerPlant(name), ctx, state) => ctx.invalidCommand("Power plant already exists!")
      }

      /*
      Capacity changes
      TODO: DRY this out!
       */
      .onCommand[ChangeEnergyCapacity, Done] {
        case (ChangeEnergyCapacity(newCapacity), ctx, state) if newCapacity < state.powerPlant.get.energy =>
          throw throw BadRequest("New capacity cannot be smaller than current energy reserve, try using some energy first!")
        case (ChangeEnergyCapacity(newCapacity), ctx, state) if newCapacity <= state.powerPlant.get.generator.rate =>
          throw throw BadRequest("New capacity must be equal or greater than the production rate of the generator.")
        case (ChangeEnergyCapacity(newCapacity), ctx, state) =>
          ctx.thenPersist(PowerPlantCapacityUpdated(newCapacity))(_ => ctx.reply(Done))
      }

      /*
      Energy generation
       */
      .onCommand[ProduceEnergy.type , Done] {
        case (ProduceEnergy, ctx, state) if (state.powerPlant.get.energy + state.powerPlant.get.generator.rate) >= state.powerPlant.get.capacity =>
          throw throw BadRequest("Cannot generate more energy: maximum capacity reached!")
        case (ProduceEnergy, ctx, state) =>
          // generate() only returns the production rate now, but it could contain more complex logic or even finish later
          val producedEnergy = state.powerPlant.get.generator.generate()
          ctx.thenPersist(EnergyProduced(producedEnergy))(_ => ctx.reply(Done))
      }

      /*
      Energy consumption
       */
      .onCommand[ConsumeEnergy, Done] {
        case (ConsumeEnergy(energy), ctx, state) if energy.kw <= 0 =>
          throw throw BadRequest("Kilowatts cannot be zero or negative. You cannot consume nothing...")
        case (ConsumeEnergy(energy), ctx, state) if (state.powerPlant.get.energy - energy).kw < 0 =>
          throw throw BadRequest("Insufficient energy!")
        case (ConsumeEnergy(energy), ctx, state) =>
          ctx.thenPersist(EnergyConsumed(energy))(_ => ctx.reply(Done))
      }

      /*
      Events
       */
      .onEvent {
        case (PowerPlantCapacityUpdated(newCapacity), state) => state.withCapacity(newCapacity)
        case (EnergyProduced(energy, timestamp), state) => state.produce(timestamp, energy)
        case (EnergyConsumed(energy, timestamp), state) => state.consume(timestamp, energy)
      }
  }
}

// TODO: move / wrong place (why doesn't lagom include this exception anyway?)
final class BadRequest(errorCode: TransportErrorCode, exceptionMessage: ExceptionMessage, cause: Throwable) extends TransportException(errorCode, exceptionMessage, cause) {
  def this(errorCode: TransportErrorCode, exceptionMessage: ExceptionMessage) = this(errorCode, exceptionMessage, null)
}
object BadRequest {
  val ErrorCode = TransportErrorCode.BadRequest

  def apply(message: String) = new BadRequest(
    ErrorCode,
    new ExceptionMessage(classOf[BadRequest].getSimpleName, message), null
  )

  def apply(cause: Throwable) = new PolicyViolation(
    ErrorCode,
    new ExceptionMessage(classOf[BadRequest].getSimpleName, cause.getMessage), cause
  )
}

/**
  * An object encapsulating the state of this persistent entity.
  * Usage history facilitates graph plotting
  * @param powerPlant
  * @param usageHistory
  */
case class PowerPlantState(powerPlant: Option[PowerPlant], usageHistory: Seq[PowerUsage]) {

  def withCapacity(power: Kilowatts): PowerPlantState =
    copy(Option(powerPlant.get.copy(capacity = power)))

  def produce(timestamp: Instant, producedEnergy: Kilowatts): PowerPlantState =
    copy(
      powerPlant = Some(powerPlant.get.copy(energy = powerPlant.get.energy + producedEnergy)),
      usageHistory = usageHistory :+ PowerUsage(timestamp, producedEnergy, Kilowatts(0), usageHistory.lastOption.getOrElse(PowerUsage.empty).total + producedEnergy))

  def consume(timestamp: Instant, consumedEnergy: Kilowatts): PowerPlantState =
    copy(
      powerPlant = Some(powerPlant.get.copy(energy = powerPlant.get.energy - consumedEnergy)),
      usageHistory = usageHistory :+ PowerUsage(timestamp, Kilowatts(0), consumedEnergy, usageHistory.lastOption.getOrElse(PowerUsage.empty).total - consumedEnergy))
}

object PowerPlantState {
  implicit val format: Format[PowerPlantState] = Json.format
  val newEntity = PowerPlantState(None, Seq.empty[PowerUsage])
}

final case class PowerUsage(instant: Instant, produce: Kilowatts, consume: Kilowatts, total: Kilowatts = Kilowatts(0))
object PowerUsage {
  implicit val format: Format[PowerUsage] = Json.format
  def empty: PowerUsage = PowerUsage(Instant.MIN, Kilowatts(0), Kilowatts(0), Kilowatts(0))
}

/*
Commands
 */

sealed trait PowerPlantCommand[R] extends ReplyType[R]

final case class CreatePowerPlant(plant: PowerPlant) extends PowerPlantCommand[Done]
object CreatePowerPlant {
  implicit val format: Format[CreatePowerPlant] = Json.format
}

object GetPowerPlant extends PowerPlantCommand[Option[PowerPlant]] {
  implicit val format: Format[GetPowerPlant.type] = JsonSerializer.emptySingletonFormat(GetPowerPlant)
}

final case class ChangeEnergyCapacity(newCapacity: Kilowatts) extends PowerPlantCommand[Done]
object ChangeEnergyCapacity {
  implicit val format: Format[ChangeEnergyCapacity] = Json.format
}

object ProduceEnergy extends PowerPlantCommand[Done] {
  implicit val format: Format[ProduceEnergy.type] = JsonSerializer.emptySingletonFormat(ProduceEnergy)
}

final case class ConsumeEnergy(energy: Kilowatts) extends PowerPlantCommand[Done]
object ConsumeEnergy {
  implicit val format: Format[ConsumeEnergy] = Json.format
}

/*
Events
 */

sealed trait PowerPlantEvent

final case class PowerPlantedCreated(plant: PowerPlant) extends PowerPlantEvent
object PowerPlantedCreated {
  implicit val format: Format[PowerPlantedCreated] = Json.format
}

final case class PowerPlantCapacityUpdated(newCapacity: Kilowatts) extends PowerPlantEvent
object PowerPlantCapacityUpdated {
  implicit val format: Format[PowerPlantCapacityUpdated] = Json.format
}

final case class EnergyProduced(energy: Kilowatts, time: Instant = Instant.now) extends PowerPlantEvent
object EnergyProduced {
  implicit val format: Format[EnergyProduced] = Json.format
}

final case class EnergyConsumed(energy: Kilowatts, time: Instant = Instant.now) extends PowerPlantEvent
object EnergyConsumed {
  implicit val format: Format[EnergyConsumed] = Json.format
}

/**
  * Akka serialization, used by both persistence and remoting, needs to have
  * serializers registered for every type serialized or deserialized. While it's
  * possible to use any serializer you want for Akka messages, out of the box
  * Lagom provides support for JSON, via this registry abstraction.
  *
  * The serializers are registered here, and then provided to Lagom in the
  * application loader.
  */
object PowerPlantSerializerRegistry extends JsonSerializerRegistry {
  override def serializers: Seq[JsonSerializer[_]] = Seq(

    // Commands
    JsonSerializer[CreatePowerPlant],
    JsonSerializer[GetPowerPlant.type],
    JsonSerializer[ChangeEnergyCapacity],
    JsonSerializer[ProduceEnergy.type],
    JsonSerializer[ConsumeEnergy],

    // Events
    JsonSerializer[PowerPlantedCreated],
    JsonSerializer[PowerPlantCapacityUpdated],
    JsonSerializer[EnergyProduced],
    JsonSerializer[EnergyConsumed]
  )
}