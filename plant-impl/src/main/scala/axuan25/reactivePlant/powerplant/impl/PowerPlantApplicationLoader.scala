package axuan25.reactivePlant.powerplant.impl

import axuan25.reactivePlant.powerplant.api.PowerPlantService
import com.lightbend.lagom.scaladsl.api.ServiceLocator
import com.lightbend.lagom.scaladsl.devmode.LagomDevModeComponents
import com.lightbend.lagom.scaladsl.persistence.cassandra.CassandraPersistenceComponents
import com.lightbend.lagom.scaladsl.server.{LagomApplication, LagomApplicationContext, LagomApplicationLoader, LagomServer}
import com.softwaremill.macwire.wire
import play.api.libs.ws.ahc.AhcWSComponents

abstract class PowerPlantApplication(context: LagomApplicationContext)
  extends LagomApplication(context)
    with AhcWSComponents
    with CassandraPersistenceComponents {

  override lazy val lagomServer = LagomServer.forServices(
    bindService[PowerPlantService].to(wire[PowerPlantServiceImpl])
  )
  override lazy val jsonSerializerRegistry = PowerPlantSerializerRegistry

  persistentEntityRegistry.register(wire[PowerPlantEntity])
}

class PowerPlantApplicationLoader extends LagomApplicationLoader {

  override def load(context: LagomApplicationContext) =
    new PowerPlantApplication(context) {
      override def serviceLocator = ServiceLocator.NoServiceLocator
    }

  override def loadDevMode(context: LagomApplicationContext) =
    new PowerPlantApplication(context) with LagomDevModeComponents

  override def describeServices = List(
    readDescriptor[PowerPlantService]
  )
}