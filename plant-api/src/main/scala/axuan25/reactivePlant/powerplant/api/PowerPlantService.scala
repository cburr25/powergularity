package axuan25.reactivePlant.powerplant.api

import java.util.UUID

import akka.{Done, NotUsed}
import com.lightbend.lagom.scaladsl.api.transport.Method
import com.lightbend.lagom.scaladsl.api.{Descriptor, Service, ServiceCall}

import scala.collection.immutable.Seq


trait PowerPlantService extends Service {

  def create: ServiceCall[PowerPlant, PowerPlant]

  def getAll: ServiceCall[NotUsed, Seq[PowerPlant]]

  def getOne(plantId: UUID): ServiceCall[NotUsed, PowerPlant]

  def updatePlantCapacity(plantId: UUID): ServiceCall[Kilowatts, Done]

  def produceEnergy(plantId: UUID): ServiceCall[NotUsed, Done]

  def consumeEnergy(plantId: UUID): ServiceCall[Kilowatts, Done]

  override def descriptor(): Descriptor = {
    import Service._
    named("powerplant-service").withCalls(
      restCall(Method.POST, "/api/plants", create),
      pathCall("/api/plants", getAll _),
      pathCall("/api/plants/:id", getOne _),
      restCall(Method.PATCH, "/api/plants/:id", updatePlantCapacity _),
      restCall(Method.PATCH, "/api/plants/:id/produce", produceEnergy _),
      restCall(Method.POST, "/api/plants/:id/consume ", consumeEnergy _)
    ).withAutoAcl(true)
  }
}
