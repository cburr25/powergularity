package axuan25.reactivePlant.powerplant.api

import java.time.Instant
import java.util.UUID

import play.api.libs.json._

/**
  * A value object representing power in kilowatts
  * @param kw
  */
case class Kilowatts(kw: Int) extends AnyVal {

  // TODO/fixme/study: proper value object validation practices
//  if (kw < 0) throw new IllegalArgumentException("Power value cannot be negative.")

  def +(p: Kilowatts): Kilowatts = Kilowatts(kw + p.kw)
  def -(p: Kilowatts): Kilowatts = Kilowatts(kw - p.kw)

  def <(p: Kilowatts): Boolean = kw < p.kw
  def <=(p: Kilowatts): Boolean = kw <= p.kw
  def >(p: Kilowatts): Boolean = kw > p.kw
  def >=(p: Kilowatts): Boolean = kw >= p.kw
}
object Kilowatts {
  implicit val reads = (__ \ "key").read[Int].map(Kilowatts.apply)
  implicit val format: Format[Kilowatts] = Json.format
}

sealed trait PowerGenerator {
  def generate(): Kilowatts = rate
  val rate: Kilowatts
}

case object SolarPanel extends PowerGenerator {
  override val rate: Kilowatts = Kilowatts(200)
}

case object WindTurbine extends PowerGenerator {
  override val rate: Kilowatts = Kilowatts(400)
}

case object CoalPlant extends PowerGenerator {
  override val rate: Kilowatts = Kilowatts(750)
}

case object NuclearReactor extends PowerGenerator {
  override val rate: Kilowatts = Kilowatts(1500)
}

object PowerGenerator {
  implicit val format = Format[PowerGenerator](
    Reads { js =>
      // `generatorType` is used for deserialization
      val generatorType = (JsPath \ "generatorType").read[String].reads(js)
      generatorType.fold(
        errors => JsError("generatorType undefined or incorrect"), {
          case "windTurbine" => JsSuccess(WindTurbine)
          case "solarPanel" => JsSuccess(SolarPanel)
          case "coalPlant" => JsSuccess(CoalPlant)
          case "nuclearReactor" => JsSuccess(NuclearReactor)
        }
      )
    },
    Writes {
      case WindTurbine=> JsObject(Seq("generatorType" -> JsString("windTurbine")))
      case SolarPanel => JsObject(Seq("generatorType" -> JsString("solarPanel")))
      case CoalPlant => JsObject(Seq("generatorType" -> JsString("coalPlant")))
      case NuclearReactor=> JsObject(Seq("generatorType" -> JsString("nuclearReactor")))
    }
  )
}

/**
  * The PowerPlant Aggregate Root
  *
  * @param id
  * @param owner
  * @param generator
  * @param energy
  * @param capacity
  * @param lastProduced
  * @param lastConsumed
  */
case class PowerPlant (
                        id: Option[UUID],
                        owner: String,
                        generator: PowerGenerator,
                        energy: Kilowatts,
                        capacity: Kilowatts,
                        lastProduced: Option[Instant] = Some(Instant.now),
                        lastConsumed: Option[Instant]) {

  def this(owner: String, generator: PowerGenerator) = this(None, owner, generator, Kilowatts(0), generator.rate, None, None)

  /**
    * Because the plant cannot be charged over capacity,
    * it is considered full when and if a single charge would
    * cause the new value to be >= the capacity.
    *
    * @return true if the current energy + 1 charge >= capacity
    */
  def isFull: Boolean = (energy.kw + generator.rate.kw) >= capacity.kw
}

object PowerPlant {
  import play.api.libs.json._
  import play.api.libs.functional.syntax._

  def partialApply(owner: String, generator: PowerGenerator) = new PowerPlant(owner, generator)

  /**
    * Define default ordering for listings
    */
  implicit object PlantOrdering extends Ordering[PowerPlant] {
    override def compare(x: PowerPlant, y: PowerPlant): Int = {
      x.lastConsumed.getOrElse(Instant.now()).compareTo(y.lastConsumed.getOrElse(Instant.now()))
    }
  }

  // TODO/fixme: this should technically work to accept partial json objects, yet the entire object is still expected during unmarshalling
  implicit val reads: Reads[PowerPlant] = (
    (__ \ "owner").read[String] and
      (__ \ "generator").read[PowerGenerator]
    )(PowerPlant.partialApply _)

  implicit val format: Format[PowerPlant] = Json.format
}