This sample project was created to learn the following new technologies to me:

- Scala
- Microservices à la DDD
- CQRS + Event Sourcing architecture

After weeks of reading and exploring various approaches, I have chosen the Lagom framework by Lightbend. The Lagom framework is an opiniated framework that seeks to
facilitate the development of microservices; it is built on top of Play! framework and Akka and offers both a Java DSL and a Scala DSL.


_04/19/2017: I will be polishing a few aspects of the demo very soon and place comments as much as possible, watch or come back later!_

# Left to do:

- Authentication service
- Authorization roles & permissions: users should not be able to modify other users' data
- DevOps / containerization to demonstrate various deployment strategies
- Streams
- Read side using an Event Processor and Cassandra

# Inspirations / Help:

The following are links to books, articles, tutorials, other sample projects as well as conferences (NDC, Devoxx, GOTO; etc) that have guided me in my learning.

TODO: complete

Vaughn Vernon DDD microservices using Akka

Renato Devoxx talk

Chris Richardson DDD talks + Eventuate.io

Martin Fowler talks/links

Greg Young talks/articles | geteventstore