lazy val root = (project in file("."))
  .settings(name := "lagom-power-plant-scala")
  .aggregate(userApi, userImpl, plantApi, plantImpl)
  .settings(commonSettings: _*)
  .settings(
    libraryDependencies ++= Seq(
       "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.8.8" // actually, only api projects need this
    )
  )

organization in ThisBuild := "axuan25.reactive-plant"

// the Scala version that will be used for cross-compiled libraries
scalaVersion in ThisBuild := "2.11.8"

version in ThisBuild := "1.0-SNAPSHOT"

val playJsonDerivedCodecs = "org.julienrf" %% "play-json-derived-codecs" % "3.3"
val macwire = "com.softwaremill.macwire" %% "macros" % "2.2.5" % "provided"
val scalaTest = "org.scalatest" %% "scalatest" % "3.0.1" % "test"

/*lazy val security = (project in file("security"))
  .settings(commonSettings: _*)
  .settings(
    version := "1.0-SNAPSHOT",
    libraryDependencies ++= Seq(
      lagomScaladslApi,
      lagomScaladslServer % Optional,
      playJsonDerivedCodecs,
      scalaTest
    )
  )*/

lazy val userApi = (project in file("user-api"))
  .settings(commonSettings: _*)
  .settings(
    version := "1.0-SNAPSHOT",
    libraryDependencies ++= Seq(
      lagomScaladslApi,
      playJsonDerivedCodecs
    )
  )
//  .dependsOn(security)

lazy val userImpl = (project in file("user-impl"))
  .dependsOn(userApi)
  .settings(commonSettings: _*)
  .enablePlugins(LagomScala)
  .settings(
      version := "1.0-SNAPSHOT",
      libraryDependencies ++= Seq(
        lagomScaladslPersistenceCassandra,
        lagomScaladslTestKit,
        lagomScaladslKafkaBroker,
        "com.datastax.cassandra" % "cassandra-driver-extras" % "3.0.0",
        macwire,
        scalaTest
      )
  )
  .settings(lagomForkedTestSettings: _*)
//  .dependsOn(userApi, plantApi)

lazy val plantApi = (project in file("plant-api"))
  .settings(commonSettings: _*)
  .settings(
    version := "1.0-SNAPSHOT",
    libraryDependencies ++= Seq(
      lagomScaladslApi,
      playJsonDerivedCodecs
    )
  )
//  .dependsOn(security)

lazy val plantImpl = (project in file("plant-impl"))
  .settings(commonSettings: _*)
  .enablePlugins(LagomScala)
  .settings(
    version := "1.0-SNAPSHOT",
    libraryDependencies ++= Seq(
      lagomScaladslPersistenceCassandra,
      lagomScaladslTestKit,
      lagomScaladslKafkaBroker,
      "com.datastax.cassandra" % "cassandra-driver-extras" % "3.0.0",
      macwire,
      scalaTest
    )
  )
  .settings(lagomForkedTestSettings: _*)
  .dependsOn(plantApi)

def commonSettings: Seq[Setting[_]] = Seq(
  scalacOptions in Compile += "-Xexperimental" // this enables Scala lambdas to be passed as Java SAMs
)
lagomCassandraCleanOnStart in ThisBuild := false


// TODO: clean-up

/*
Cassandra settings
 */

// do not delete database files on start
//lagomCassandraCleanOnStart in ThisBuild := false

// un-comment to disable embbeded cassandra server
//lagomCassandraEnabled in ThisBuild := false

// local cassandra server
//lagomUnmanagedServices in ThisBuild := Map("cas_native" -> "http://localhost:9042")

//lagomCassandraJvmOptions in ThisBuild :=
//  Seq("-Xms256m", "-Xmx1024m", "-Dcassandra.jmx.local.port=4099",
//    "-DCassandraLauncher.configResource=dev-embedded-cassandra.yaml") // these are actually the default jvm options

// Configure max start time for cassandra
//import scala.concurrent.duration._ // Mind that the import is needed.
//lagomCassandraMaxBootWaitingTime in ThisBuild := 0.seconds

/*
Kafka settings
 */

//lagomKafkaPropertiesFile in ThisBuild :=
//  Some((baseDirectory in ThisBuild).value / "project" / "kafka-server.properties")

//lagomKafkaJvmOptions in ThisBuild := Seq("-Xms256m", "-Xmx1024m") // these are actually the default jvm options

// un-comment to use custom kafka server
//lagomKafkaEnabled in ThisBuild := false
//lagomKafkaAddress in ThisBuild :=  "localhost:10000"